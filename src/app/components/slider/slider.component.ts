import { Component, OnInit, OnDestroy } from '@angular/core';
import { SliderModel } from 'src/app/models/slider.model';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit, OnDestroy {
  public images: SliderModel[] = [
    { imageUrl: 'https://www.w3schools.com/w3css/img_chicago.jpg', alt: 'rock image' },
    { imageUrl: 'https://www.w3schools.com/w3css/img_la.jpg', alt: 'rock image' },
    { imageUrl: 'https://www.w3schools.com/w3css/img_ny.jpg', alt: 'rock image' },
  ];

  public currentImageIndex = 0;

  private timeId;

  ngOnInit() {
    this.carousel();
  }

  private carousel() {
    this.timeId = setInterval(() => {
      if (this.images.length - 1 === this.currentImageIndex) {
        this.currentImageIndex = 0;

        return;
      }

      this.currentImageIndex++;
    }, 3000);
  }

  ngOnDestroy(): void {
    clearInterval(this.timeId);
  }

}
