import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HeaderComponent } from './components/header/header.component';
import { SliderComponent } from './components/slider/slider.component';
import { AdvantagesComponent } from './components/advantages/advantages.component';
import { PopularArticlesComponent } from './components/popular-articles/popular-articles.component';
import { OurClientsComponent } from './components/our-clients/our-clients.component';
import { OurTeamComponent } from './components/our-team/our-team.component';
import { SubscribeFormComponent } from './components/subscribe-form/subscribe-form.component';
import { FooterComponent } from './components/footer/footer.component';
import { OurProductComponent } from './components/our-product/our-product.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    HeaderComponent,
    SliderComponent,
    AdvantagesComponent,
    PopularArticlesComponent,
    OurClientsComponent,
    OurTeamComponent,
    SubscribeFormComponent,
    FooterComponent,
    OurProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
